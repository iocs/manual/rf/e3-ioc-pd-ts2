require essioc
require piezodriver

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("PREFIX", "TS2-010RFC:RFS-PZC-101")
epicsEnvSet("PORT", "piezoPort")
epicsEnvSet("DEVICE_NAME","/dev/xdma6")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES","20000000")

# Load piezodriver startup script
iocshLoad("$(piezodriver_DIR)/piezo.iocsh", "P=$(PREFIX):, R=, PORT=$(PORT), DEVICE_NAME=$(DEVICE_NAME), TIMEOUT=1")

